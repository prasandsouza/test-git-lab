const fs = require('fs');
const Papa = require('papaparse');

const csvFilePath = 'matches.csv'

const file = fs.createReadStream(csvFilePath);
let data = []

Papa.parse(file, {
    header : true,
    download:true,
     step : function(results) {
        data.push(results.data)
    }
});


let result = data.reduce((finalValue,currentValue)=>{
    if(finalValue.hasOwnProperty(currentValue.season)){
        finalValue[currentValue.season] +=1; 
    } else { 
        finalValue[(currentValue.season)] = 1; 
    }
    return finalValue
},{})
console.log(result)
